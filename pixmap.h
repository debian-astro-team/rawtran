/*

  pixmap -  Decode pixmap
  Copyright (C) 2015  Filip Hroch, Masaryk University, Brno, CZ

  Rawtran is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Rawtran is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Rawtran.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <assert.h>

typedef struct {
  int width, height;
  int maxval;
  int colors;
  unsigned short int *data;
  /* Data contains one or tree color bands. The bands are stored in
     a plain one-dimensional array. Single bands are stored sequently
     i order: R G B (X Y Z). Any sub-array has width*height elements.
     Star positions of every band is on width*height*(order) place.
     An origin is left bottom corner (as usuall in FITS).
  */
} PIXMAP;

extern PIXMAP *pixmap(FILE *);
extern PIXMAP *pixmap_init(int,int,int,int);
extern void pixmap_free(PIXMAP *);
extern void pixmap_save(PIXMAP *, char *);
extern int pixmap_ppm2pgm(char *, char*);
